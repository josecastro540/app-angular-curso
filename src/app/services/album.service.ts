import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http'
import 'rxjs/add/operator/map';
import {GLOBAL} from './GLOBAL';
import { Album } from './../models/album.model';


@Injectable()
export class AlbumService {
  public url:string;
  public loading:boolean = false;
  public errMsj:string;

  constructor( private http:Http ) {
    this.url = GLOBAL.url;
  }

  getAlbums(){
    let url = this.url + 'albums';
    // let headers = new Headers({
    //   'Content-Type': 'application/json'
    // })
    return this.http.get(url)
    .map(res=>{
      return res.json()
    })
  }

  getAlbum(id:string){
    let url = this.url +'album/' + id;
    return this.http.get(url)
      .map(res=>{
        return res.json()
      })
  }

  addAlbum(formData:FormData){
    let url = this.url +'create-album';
    let headers = new Headers(
    //   {
    //   'Content-Type': 'application/json'
    // }
  );
    // let payload = album;
    return  this.http.post(url, formData, {headers})
      .map(res =>{
        return res.json();
      })
  }

  editAlbum(id:string, formData:FormData){
    let url = this.url + 'edit-album/' + id;
    let headers = new Headers(
    //   {
    //   'Content-Type': 'application/json'
    // }
  );

    return this.http.put(url,formData, {headers})
      .map(res=>{
        return res.json();
      })
  }

  deleteAlbum(id:string){
    let url = this.url + 'delete-album/' + id;
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.delete(url, {headers})
      .map(res=>{
        return res.json();
      })
  }

}
