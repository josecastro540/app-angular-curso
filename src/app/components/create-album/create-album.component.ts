import { Album } from './../../models/album.model';
import { AlbumService } from './../../services/album.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'


@Component({
  selector: 'app-create-album',
  templateUrl: './create-album.component.html',
  styleUrls: ['./create-album.component.css']
})
export class CreateAlbumComponent implements OnInit {
  public album:Album = new Album();
  public title:string = 'Crear Album';
  public file:File;
  public preview: FileReader;
  public formData:FormData;

  constructor( public _albumService:AlbumService,
                           public router:Router ) { }

  ngOnInit() {
  }

  onSubmit(){
    this._albumService.loading = true;
    //form data
    this.formData = new FormData();

    this.formData.append('titulo', this.album.titulo);
    this.formData.append('descripcion', this.album.descripcion);
    this.formData.append('fecha', this.album.fecha);
    //Imagen
    if(this.file){
      this.formData.append('portada', this.file, this.file.name);
    }


    this._albumService.addAlbum(this.formData)
      .subscribe(data=>{
        console.log(data);
        this._albumService.loading = false;
        this.router.navigate(['/albums'])
      }, err=>{
         this._albumService.loading = false;
        console.log(err)
      })
  }


  getPortada($event){
    let fileList: FileList = $event.target.files;
    this._albumService.errMsj = "";
    // console.log($event.target.files[0])
    if(fileList.length > 0){
      this.file = fileList[0];

      //Validar tamaño de archivo
      let tamano = this.file.size/1000000;
      console.log(tamano)
      if(tamano > 2){
        this._albumService.errMsj ="La imagen no puede pesar mas de 2Mb";
        return;
      }else{
        this._albumService.errMsj = "";
      }

       //Validar tamaño de archivo
       let name = this.file.name;
       let cadena = name.split('.');
       let ext = cadena.pop();
       console.log(ext)
       if(ext != 'jpg' &&  ext != 'jpeg' && ext != 'png' &&  ext != 'gif'){
         this._albumService.errMsj ="La imagen debe tener un formato permitido";
         return;
       }else{
         this._albumService.errMsj = "";
       }

       //Validar tipo de archivo



      this.preview = new FileReader();

      this.preview.readAsDataURL(this.file);
      this.preview.onloadend = ()=>{
        document.getElementById('preview').setAttribute('src', this.preview.result);
      }

    }

  }

}
