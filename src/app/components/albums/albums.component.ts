import { Album } from './../../models/album.model';
import { AlbumService } from './../../services/album.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})

export class AlbumsComponent implements OnInit {
  public albums:Album[];
  public mostrar:boolean;


  constructor( public _albumService:AlbumService ) { }

  ngOnInit() {
    this.obtenerAlbums();
  }

  obtenerAlbums(){
    this._albumService.loading = true;
    this._albumService.getAlbums()
    .subscribe(data=>{
      console.log(data)
      this.albums = data.albums;
      this._albumService.loading = false;
    },err=>{
      console.log(err);
      this._albumService.loading = false;
    })
  }

  borrarAlbum(id:string){
    this._albumService.deleteAlbum(id)
      .subscribe(data=>{
        this.obtenerAlbums();
      },err=>{
        console.log(err);
      })
  }

  mostrarConfirm(id){
    this.mostrar = id;
  }

  onCancelConfirm(){
    this.mostrar = null;
  }

}
