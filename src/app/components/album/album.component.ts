import { Album } from './../../models/album.model';
import { AlbumService } from './../../services/album.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute  } from '@angular/router';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {
  public album:Album;

  constructor( public _albumService:AlbumService,
                            public ac:ActivatedRoute ) { }

  ngOnInit() {
    this.obtenerAlbum();
  }

  obtenerAlbum(){
    this._albumService.loading = true;
    this.ac.params
      .subscribe(params=>{
        console.log(params['id'])
        this._albumService.getAlbum(params['id'])
        .subscribe(data=>{

          console.log(data);
          this.album = data.album;
          this._albumService.loading = false;
        }, err=>{
          console.log(err);
          this._albumService.loading = false;
        })
      })
  }

}
