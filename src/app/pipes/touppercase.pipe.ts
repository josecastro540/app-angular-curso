import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'touppercase'
})
export class TouppercasePipe implements PipeTransform {

  transform(value: string): string {
    let upper:string = value.toUpperCase();
    return upper;
  }

}
