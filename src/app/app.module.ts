import { AlbumService } from './services/album.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


//Rutas
import { APP_ROUTING } from './app.routes';

import { AppComponent } from './app.component';
import { AlbumsComponent } from './components/albums/albums.component';
import { AlbumComponent } from './components/album/album.component';
import { CreateAlbumComponent } from './components/create-album/create-album.component';
import { EditAlbumComponent } from './components/create-album/edit-album.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { TouppercasePipe } from './pipes/touppercase.pipe';

@NgModule({
  declarations: [
    AppComponent,
    AlbumsComponent,
    AlbumComponent,
    CreateAlbumComponent,
    EditAlbumComponent,
    NavbarComponent,
    TouppercasePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    APP_ROUTING
  ],
  providers: [AlbumService],
  bootstrap: [AppComponent]
})
export class AppModule { }
