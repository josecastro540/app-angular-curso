import { EditAlbumComponent } from './components/create-album/edit-album.component';
import { AlbumComponent } from './components/album/album.component';
import { RouterModule, Routes } from '@angular/router'
import { CreateAlbumComponent } from './components/create-album/create-album.component';
import { AlbumsComponent } from './components/albums/albums.component';


const APP_ROUTES:Routes =[
  {path: 'albums', component:AlbumsComponent},
  {path: 'album/:id', component:AlbumComponent},
  {path: 'create-album', component:CreateAlbumComponent},
  {path: 'edit-album/:id', component:EditAlbumComponent},
  {path: '**',pathMatch:'full', redirectTo:'albums'}
]

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES)
